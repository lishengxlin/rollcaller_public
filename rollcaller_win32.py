import sys
import time
import random
import os

#	 This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

print("欢迎使用RollCaller脚本!")
print("\033[32m=======================Author=======================")
print("Gitlab:https://gitlab.com/lishengxlin")
print("Github:https://github.com/lishengxlin")
print("基于GPLv3(GNU General Public License Version 3)分发")
print("====================================================\033[0m")
print("请确保列表文件与本脚本置于同一路径下\n你可以通过修改源码来改变要读取的文件名（默认：listfile）")

# 正片开始

# 初始化列表
file = r"listfile.txt"
a = []

# 检查文件是否存在
# 存在则向a添加列表，不存在则触发exit 1
try:
	b = open(file,'r')
	print('Successfully open %s' %file)
	for line in b.readlines():
		line = line.replace('\n','')
		line = line.split(",")
		a.append(line)
		num = len(a)
	b.close()
except IOError:
	print("%s Not exist!" %file)
	sys.exit(1)


# 开始执行主代码

# 询问用户是否开始
input_a = input("开始？[(O)K / Else for quit.] :")
if str.lower(input_a) == "o":
	print("\033[32m脚本开始\033[0m")
	time.sleep(0.5)
else:
	print("退出")
	sys.exit(0)

# 用户选择开始
input_b = "u"
while input_b == "u":
#	print("\033[1;32m按下组合键CTRL+C(键盘中断)以停止洗牌\033[0m")
	time.sleep(0.3)
	random.shuffle(a)
	key_1 = "u"
	while key_1 == "u":
		try:
			for i in range(0, num - 1):
				o = "\033[1;32m按下组合键CTRL+C(键盘中断)以停止洗牌\033[0m" + str("(｡•̀ᴗ-)✧ ") + str(''.join(a[i]))
				print(f"\n\r{o}", end=" ")
				time.sleep(0.03)
		except KeyboardInterrupt:
			key_1 = "v"
			os.system("cls")
			continue
	rnum = random.randint(0, num - 1 )
	result = ''.join(a[rnum])
	print(f"\r\033[2;4;32m#PLACEHOLDER//KEYBOARD INTERRUPTED(WHILE LOOP)//PLACEHOLDER#\033[0m", end=" ")
	print(f"\n\r\033[41;1m%s\033[0m  \033[33m被选中\033[0m" %result, end=" ")
	time.sleep(0.3)
	input_b = str.lower(input("\n再来一次？[(A)gain / (Q)uit ]:"))
	if input_b == "a":
		input_b = "u"
		continue
	elif input_b == "q":
		break
	else:
		while input_b != "a" or input != "q":
			print("\033[31m%s: Unknown Command\033[0m" %input_b)
			input_b = str.lower(input("再来一次？[(A)gain / (Q)uit ]:"))
			if input_b == "a":
				input_b = "u"
				break
			elif input_b == "q":
				input_b = "q"
				break
			else:
				continue
print("Exited")
